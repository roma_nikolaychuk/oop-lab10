﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class Fraction
    {
        private double _value;

        public double Value
        {
            get { return _value; }
            set { _value = value; }
        }

        private double numerator;

        public double Numerator
        {
            get { return numerator; }
            set { numerator = value; }
        }

        private double denominator;

        public double Denominator
        {
            get { return denominator; }
            set { denominator = value; }
        }

        public Fraction()
        {

        }

        public Fraction(double numerator, double denominator)
        {
            this.numerator = numerator;
            this.denominator = denominator;
        }

        public Fraction(Fraction fraction)
        {
            this.Numerator = fraction.Numerator;
            this.Denominator = fraction.Denominator;
        }

        public static Fraction operator +(Fraction f1, Fraction f2)
        {
            return new Fraction { Value = f1.Numerator + f2.Denominator };
        }

        public static Fraction operator -(Fraction f1, Fraction f2)
        {
            return new Fraction { Value = f1.Numerator - f2.Denominator };
        }

        public static Fraction operator *(Fraction f1, Fraction f2)
        {
            return new Fraction { Value = f1.Numerator * f2.Denominator };
        }

        public static Fraction operator /(Fraction f1, Fraction f2)
        {
            return new Fraction { Value = f1.Numerator / f2.Denominator };
        }

        public static bool operator >(Fraction f1, Fraction f2)
        {
            if (f1.Numerator > f2.Denominator)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool operator <(Fraction f1, Fraction f2)
        {
            if (f1.Numerator < f2.Denominator)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool operator <=(Fraction f1, Fraction f2)
        {
            if (f1.Numerator <= f2.Denominator)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool operator >=(Fraction f1, Fraction f2)
        {
            if (f1.Numerator >= f2.Denominator)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool operator ==(Fraction f1, Fraction f2)
        {
            if (f1.Numerator == f2.Denominator)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool operator !=(Fraction f1, Fraction f2)
        {
            if (f1.Numerator != f2.Denominator)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static implicit operator Fraction(int f1)
        {
            return new Fraction { Numerator = f1 };
        }

        public static implicit operator double(Fraction f1)
        {
            return f1.Numerator;
        }
    }
}
