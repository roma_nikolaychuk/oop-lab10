﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary;

namespace oop_lab10
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
            System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

            Fraction f1 = new Fraction {};
            Fraction f2 = new Fraction {};
            
            Console.Write("Numerator = ");
            f1.Numerator = Convert.ToDouble(Console.ReadLine());
            Console.Write("Denominator = ");
            f2.Denominator = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine();

            Fraction f3 = f1 + f2;
            Console.WriteLine("Додавання:");
            Console.WriteLine("Numerator + Denominator = " + f3.Value);
            Console.WriteLine();

            f3 = f1 - f2;
            Console.WriteLine("Віднімання:");
            Console.WriteLine("Numerator - Denominator = " + f3.Value);
            Console.WriteLine();

            f3 = f1 * f2;
            Console.WriteLine("Множення:");
            Console.WriteLine("Numerator * Denominator = " + f3.Value);
            Console.WriteLine();

            f3 = f1 / f2;
            Console.WriteLine("Numerator / Denominator:");
            Console.WriteLine("Value = " + f3.Value);
            Console.WriteLine();

            bool f4 = f1 > f2;
            Console.WriteLine("Numerator > Denominator:");
            Console.WriteLine("Value = " + f4);
            Console.WriteLine();

            f4 = f1 < f2;
            Console.WriteLine("Numerator < Denominator:");
            Console.WriteLine("Value = " + f4);
            Console.WriteLine();

            f4 = f1 >= f2;
            Console.WriteLine("Numerator >= Denominator:");
            Console.WriteLine("Value = " + f4);
            Console.WriteLine();

            f4 = f1 <= f2;
            Console.WriteLine("Numerator <= Denominator:");
            Console.WriteLine("Value = " + f4);
            Console.WriteLine();

            f4 = f1 == f2;
            Console.WriteLine("Numerator == Denominator:");
            Console.WriteLine("Value = " + f4);
            Console.WriteLine();

            f4 = f1 != f2;
            Console.WriteLine("Numerator != Denominator:");
            Console.WriteLine("Value = " + f4);
            Console.WriteLine();

            double x = f1;
            Console.WriteLine("x = " + x);

            Console.WriteLine();
            
            Console.WriteLine(f1.Numerator.ToString() + "/" + f2.Denominator.ToString());

            Console.ReadKey();
        }
    }
}
